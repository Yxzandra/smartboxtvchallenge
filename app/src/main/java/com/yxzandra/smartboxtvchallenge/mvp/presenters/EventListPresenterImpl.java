package com.yxzandra.smartboxtvchallenge.mvp.presenters;

import android.content.Context;

import com.yxzandra.smartboxtvchallenge.helpers.PreferencesManager;
import com.yxzandra.smartboxtvchallenge.mvp.models.ApiListener;
import com.yxzandra.smartboxtvchallenge.mvp.models.EventImpl;
import com.yxzandra.smartboxtvchallenge.mvp.models.EventInterface;
import com.yxzandra.smartboxtvchallenge.mvp.views.EventListView;
import com.yxzandra.smartboxtvchallenge.mvp.views.EventsAdapter;
import com.yxzandra.smartboxtvchallenge.schemas.DataEvents;

/**
 * Created by xpectra on 11/10/2017.
 */

public class EventListPresenterImpl implements EventListPresenter, ApiListener{
    private EventListView mView;
    private EventInterface eventInterface;
    private Context mContext;
    private int mPage = 1;
    private EventsAdapter mAdapter;

    @Override
    public EventListPresenter init(EventListView view, Context context) {
        mView = view;
        eventInterface = new EventImpl().init(this, context);
        mContext = context;
        return this;
    }

    @Override
    public void successApi(DataEvents result) {
        mView.hideProgress();
        if (mPage == 1)
            mAdapter = new EventsAdapter(result.getData().getItems(),mContext);
        else
            mAdapter.addAll(result.getData().getItems());

        mView.loadListEvent(mAdapter,
                result.getData().getPagination().getPage(),
                result.getData().getPagination().getLast());
    }

    @Override
    public void errorApi(int code) {
        mView.hideProgress();
        mView.showHttpError(code);
    }


    @Override
    public void loadEvent(int page) {
        mPage = page;
        eventInterface.getAllEvent(page);
    }

    @Override
    public void loadLogin() {
        mView.showProgress();
        eventInterface.postLogin();

    }
}
