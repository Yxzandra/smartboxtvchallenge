package com.yxzandra.smartboxtvchallenge.mvp.views;

/**
 * Created by xpectra on 11/10/2017.
 */

public interface EventListView {
    void showProgress();
    void hideProgress();
    void showHttpError(int code);
    void loadListEvent(EventsAdapter mAdapter, int page, int last);
    void loadNextPage();
}
