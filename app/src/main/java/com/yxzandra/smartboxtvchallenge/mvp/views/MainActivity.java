package com.yxzandra.smartboxtvchallenge.mvp.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.yxzandra.smartboxtvchallenge.R;
import com.yxzandra.smartboxtvchallenge.api.WebService;
import com.yxzandra.smartboxtvchallenge.helpers.CustomMessage;
import com.yxzandra.smartboxtvchallenge.mvp.presenters.EventListPresenter;
import com.yxzandra.smartboxtvchallenge.mvp.presenters.EventListPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements EventListView {
    @BindView(R.id.rvMatches)
    RecyclerView rvMatches;

    private EventListPresenter mPresenter;
    private MaterialDialog mDialog;
    private boolean isLastPage = false;
    private int total_page = 1;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mDialog = CustomMessage.get(this, CustomMessage.TYPE_PROGRESSBAR, getResources().getString(R.string.please_event)).build;
        rvMatches.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvMatches.setLayoutManager(linearLayoutManager);
        rvMatches.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return total_page;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        mPresenter = new EventListPresenterImpl().init(this,getBaseContext());
        mPresenter.loadLogin();
    }

    @Override
    public void showProgress() {
        mDialog.show();
    }

    @Override
    public void hideProgress() {
        mDialog.hide();
    }

    @Override
    public void showHttpError(int code) {
        WebService.handlerRequestError(this, code);
    }

    @Override
    public void loadListEvent(EventsAdapter mAdapter, int page, int last) {
        if (page == 1)
            rvMatches.setAdapter(mAdapter);

        if (page < last){
            mAdapter.addLoadingFooter();
            mAdapter.notifyDataSetChanged();
        }
        else {
            isLastPage = true;
            mAdapter.removeLoadingFooter();
            mAdapter.notifyDataSetChanged();
        }
        total_page = page;
    }

    @Override
    public void loadNextPage() {
        mPresenter.loadEvent(total_page+1);

    }
}
