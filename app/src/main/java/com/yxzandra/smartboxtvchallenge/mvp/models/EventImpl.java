package com.yxzandra.smartboxtvchallenge.mvp.models;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import com.google.gson.JsonElement;
import com.yxzandra.smartboxtvchallenge.api.WebService;
import com.yxzandra.smartboxtvchallenge.helpers.PreferencesManager;
import com.yxzandra.smartboxtvchallenge.helpers.Utils;
import com.yxzandra.smartboxtvchallenge.schemas.DataEvents;
import com.yxzandra.smartboxtvchallenge.schemas.request.RequestLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by xpectra on 11/10/2017.
 */

public class EventImpl implements EventInterface {
    private static final String TAG = EventImpl.class.getSimpleName();
    PreferencesManager preferencesManager;
    ApiListener mListener;
    Context mContext;

    @Override
    public EventInterface init(ApiListener listener, Context context) {
        mListener = listener;
        mContext = context;
        return this;
    }

    @Override
    public void getAllEvent(int page) {
        WebService.getEvents(PreferencesManager.getInstance(mContext).getToken(),
                page).enqueue(new Callback<DataEvents>() {
            @Override
            public void onResponse(Call<DataEvents> call, Response<DataEvents> response) {
                if (response.isSuccessful())
                    mListener.successApi(response.body());
                else
                    mListener.errorApi(response.code());
            }

            @Override
            public void onFailure(Call<DataEvents> call, Throwable t) {
                mListener.errorApi(400);
            }
        });


    }

    @Override
    public void postLogin() {

        String language = "es";
        String deviceId = Settings.Secure.getString(mContext.getContentResolver(),Settings.Secure.ANDROID_ID);
        String namePhone = android.os.Build.USER;
        String version = Utils.getAndroidVersion(Build.VERSION.SDK_INT);
        String width = String.valueOf(Utils.getScreenResolution(mContext).widthPixels);
        String height = String.valueOf(Utils.getScreenResolution(mContext).heightPixels);
        String modelPhone = android.os.Build.MODEL;
        String platform = "android";
        String versionApp = "1.0.0";

        RequestLogin requestLogin = new RequestLogin(language,deviceId,namePhone,version,width,height,
                modelPhone,platform,versionApp);

        WebService.postLogin(requestLogin).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    String root = response.body().getAsJsonObject().getAsJsonObject("data").get("accessToken").getAsString();
                    PreferencesManager.getInstance(mContext).setToken(root);
                    getAllEvent(1);
                }else
                    mListener.errorApi(response.code());
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                mListener.errorApi(400);
            }
        });

    }
}
