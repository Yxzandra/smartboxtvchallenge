package com.yxzandra.smartboxtvchallenge.mvp.presenters;

import android.content.Context;

import com.yxzandra.smartboxtvchallenge.mvp.views.EventListView;

/**
 * Created by xpectra on 11/10/2017.
 */

public interface EventListPresenter {
    EventListPresenter init(EventListView view, Context context);
    void loadEvent(int page);
    void loadLogin();
}
