package com.yxzandra.smartboxtvchallenge.mvp.models;

import android.content.Context;

/**
 * Created by xpectra on 11/10/2017.
 */

public interface EventInterface {
    EventInterface init(ApiListener listener, Context context);
    void getAllEvent(int page);
    void postLogin();
}
