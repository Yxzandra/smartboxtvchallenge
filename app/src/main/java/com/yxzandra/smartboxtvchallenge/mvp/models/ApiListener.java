package com.yxzandra.smartboxtvchallenge.mvp.models;

import com.yxzandra.smartboxtvchallenge.schemas.DataEvents;

/**
 * Created by xpectra on 11/10/2017.
 */

public interface ApiListener<T> {
    void successApi(DataEvents result);
    void errorApi(int code);
}

