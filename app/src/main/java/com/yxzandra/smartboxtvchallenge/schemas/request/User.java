package com.yxzandra.smartboxtvchallenge.schemas.request;

/**
 * Created by xpectra on 10/10/2017.
 */

public class User {

    private Profile profile;

    public User(Profile profile) {
        this.profile = profile;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}