package com.yxzandra.smartboxtvchallenge.schemas.request;

/**
 * Created by xpectra on 10/10/2017.
 */

public class RequestLogin {

    private User user;
    private Device device;
    private App app;

    public RequestLogin(String language, String deviceId, String phoneName, String versionPhone,
                        String widthPhone,String heigthPhone, String modelPhone, String platformPhone,
                        String versionApp) {
        this.user = new User(new Profile(language));
        this.device = new Device(deviceId,phoneName,versionPhone,widthPhone,heigthPhone,modelPhone,platformPhone);
        this.app = new App(versionApp);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

}
