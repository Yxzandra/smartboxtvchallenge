package com.yxzandra.smartboxtvchallenge.schemas.request;

/**
 * Created by xpectra on 10/10/2017.
 */

public class Profile {

    private String language;

    public Profile(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

}