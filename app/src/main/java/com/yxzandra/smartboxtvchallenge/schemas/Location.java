
package com.yxzandra.smartboxtvchallenge.schemas;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {


    @SerializedName("original")
    @Expose
    private String original;

    public Location() {
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

}
