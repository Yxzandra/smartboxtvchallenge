package com.yxzandra.smartboxtvchallenge.schemas.request;

/**
 * Created by xpectra on 10/10/2017.
 */

public class Device {

    private String deviceId;
    private String name;
    private String version;
    private String width;
    private String heigth;
    private String model;
    private String platform;

    public Device(String deviceId, String name, String version, String width, String heigth, String model, String platform) {
        this.deviceId = deviceId;
        this.name = name;
        this.version = version;
        this.width = width;
        this.heigth = heigth;
        this.model = model;
        this.platform = platform;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeigth() {
        return heigth;
    }

    public void setHeigth(String heigth) {
        this.heigth = heigth;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

}
