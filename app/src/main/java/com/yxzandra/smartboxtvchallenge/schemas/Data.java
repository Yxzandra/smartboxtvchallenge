
package com.yxzandra.smartboxtvchallenge.schemas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("sections")
    @Expose
    private List<Section> sections = null;

    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    @SerializedName("pagination")
    @Expose
    private Pagination pagination;

    public Data() {
    }

    public List<Section> getSections() {
        return sections;
    }

    public void setSections(List<Section> sections) {
        this.sections = sections;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

}
