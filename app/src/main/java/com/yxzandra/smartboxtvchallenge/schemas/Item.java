
package com.yxzandra.smartboxtvchallenge.schemas;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("belong")
    @Expose
    private Belong belong;

    @SerializedName("matchDay")
    @Expose
    private MatchDay matchDay;

    @SerializedName("eventStatus")
    @Expose
    private EventStatus eventStatus;

    @SerializedName("location")
    @Expose
    private Location location;

    @SerializedName("startDate")
    @Expose
    private String startDate;

    @SerializedName("homeTeam")
    @Expose
    private Team homeTeam;

    @SerializedName("awayTeam")
    @Expose
    private Team awayTeam;

    @SerializedName("homeScore")
    @Expose
    private Integer homeScore;

    @SerializedName("awayScore")
    @Expose
    private Integer awayScore;

    @SerializedName("matchTime")
    @Expose
    private Integer matchTime;

    @SerializedName("endDate")
    @Expose
    private String endDate;

    @SerializedName("statusDate")
    @Expose
    private String statusDate;

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Belong getBelong() {
        return belong;
    }

    public void setBelong(Belong belong) {
        this.belong = belong;
    }

    public MatchDay getMatchDay() {
        return matchDay;
    }

    public void setMatchDay(MatchDay matchDay) {
        this.matchDay = matchDay;
    }

    public EventStatus getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(EventStatus eventStatus) {
        this.eventStatus = eventStatus;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Integer awayScore) {
        this.awayScore = awayScore;
    }

    public Integer getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(Integer matchTime) {
        this.matchTime = matchTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

}
