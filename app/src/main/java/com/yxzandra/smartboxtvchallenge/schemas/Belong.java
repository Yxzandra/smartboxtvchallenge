
package com.yxzandra.smartboxtvchallenge.schemas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Belong {

    @SerializedName("tournament")
    @Expose
    private List<String> tournament = null;

    public Belong() {
    }

    public List<String> getTournament() {
        return tournament;
    }

    public void setTournament(List<String> tournament) {
        this.tournament = tournament;
    }

}
