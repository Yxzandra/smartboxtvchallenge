
package com.yxzandra.smartboxtvchallenge.schemas;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataEvents {

    @SerializedName("data")
    @Expose
    private Data data;

    public DataEvents() {
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
