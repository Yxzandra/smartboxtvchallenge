
package com.yxzandra.smartboxtvchallenge.schemas;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Name {

    @SerializedName("original")
    @Expose
    private String original;

    @SerializedName("es")
    @Expose
    private String es;

    public Name() {
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getEs() {
        return es;
    }

    public void setEs(String es) {
        this.es = es;
    }
}
