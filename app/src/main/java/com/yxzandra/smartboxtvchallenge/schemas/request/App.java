package com.yxzandra.smartboxtvchallenge.schemas.request;

/**
 * Created by xpectra on 10/10/2017.
 */

public class App {

    private String version;

    public App(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
