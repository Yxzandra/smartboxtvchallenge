package com.yxzandra.smartboxtvchallenge.api;

import com.google.gson.JsonElement;
import com.yxzandra.smartboxtvchallenge.schemas.DataEvents;
import com.yxzandra.smartboxtvchallenge.schemas.request.RequestLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by xpectra on 6/10/2017.
 */

public interface ApiInterface {
    @GET("/api/1.0/sport/events")
    Call<DataEvents> getEvents(@Header("Authorization") String bearer,
                               @Query("page") int page);

    @POST("/api/1.0/auth/users/login/anonymous")
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    Call<JsonElement> postLogin(@Header("Authorization") String basic,
                                @Body RequestLogin requestLogin);

}
